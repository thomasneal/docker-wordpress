# Docker running Wordpress + MYSQL + phpMyAdmin

Make sure Docker is installed on your machine.

Run ```docker-compose up -d``` to stand up all the containers.

Go to the port specified in your WordPress install to see it. Example: localhost:8000.

## Theme Development
If you want to add bootstrap and gulp into the mix for local development, go use:
[base-wp-gulp](https://bitbucket.org/thomasneal/base-wp-gulp).

The public folder is where all the Wordpress files should be so you can edit them.

MySQL db data will be deposited in the .data folder. If you need to wipe your db
or start over completely, make sure to delete the .data and public folders.
